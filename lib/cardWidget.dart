import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CardWidget extends StatelessWidget {
  
  final String content;

  CardWidget({
    this.content,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Center(
        child: Text("${this.content}"),
      ),
    );
  }
}