import 'dart:convert';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qr_test/cardWidget.dart';
import 'package:qr_test/qrWidget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      routes: {
        "/": (context) => MyHomePage(title: 'Flutter Demo Home Page'),
        "/order": (context) => QrWidget(),
      }
    );
  }
}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Map<String, int> _order = {};

  String _jsonString = "[]";
  String _fileName = "assets/card_list.json";

  ScanResult scanResult;

  final globalKey = GlobalKey<ScaffoldState>();

  Future<String> getTextFromFile(String fileName) async {
    return await rootBundle.loadString(fileName);
  }

  _MyHomePageState() {
    getTextFromFile(_fileName).then((val) => setState(() {
      _jsonString = val;
    }));
  }

  void incOrder(String key) {
    this._order.update(
      key,
      (value) => value + 1 ,
      ifAbsent: () => 1,
    );
  }

  Future scan() async {
    try {
      var options = ScanOptions(
        // strings: {
        //   "cancel": _cancelController.text,
        //   "flash_on": _flashOnController.text,
        //   "flash_off": _flashOffController.text,
        // },
        // restrictFormat: selectedFormats,
        // useCamera: _selectedCamera,
        // autoEnableFlash: _autoEnableFlash,
        // android: AndroidOptions(
        //   aspectTolerance: _aspectTolerance,
        //   useAutoFocus: _useAutoFocus,
        // ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        scanResult = result;
      });
    }
  }

  void onTapCard(String key) {
    globalKey.currentState.removeCurrentSnackBar();

    incOrder(key);
    print(this._order);

    globalKey.currentState.showSnackBar(
      SnackBar(
        content: Text("${key} added to the order."),
        behavior: SnackBarBehavior.floating,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    var data = jsonDecode(_jsonString);

    if (this.scanResult != null)
    {
      print("result: ${this.scanResult.rawContent}");
      dynamic resContent = json.decode(this.scanResult.rawContent);
      print("$resContent, ${resContent.runtimeType}");
      print("set data");
      data = resContent;
    } else {
      print("default data");
    }

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      key: globalKey,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_a_photo),
            tooltip: "Scan",
            onPressed: scan,
          )
        ],
      ),
      body: Center(
        child: GridView.count(
          primary: true,
          crossAxisCount: 1,
          childAspectRatio: 2/1,
          children: List.generate(data.length, (index) {
            return GestureDetector(
              onTap: () => onTapCard(data[index]["name"]),
              child: CardWidget(
                content: data[index]["name"],
              ),
            );
          }),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () { Navigator.pushNamed(
          context,
          "/order",
          arguments: this._order,
        );},
        tooltip: 'Show order',
        child: Icon(Icons.shopping_cart),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
